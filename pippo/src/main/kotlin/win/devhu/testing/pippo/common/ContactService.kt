package win.devhu.testing.pippo.common

import win.devhu.testing.pippo.Contact



interface ContactService {
    fun getContacts(): List<Contact>

    fun getContact(id: Int): Contact

    fun delete(id: Int)

    fun save(contact: Contact): Contact
}