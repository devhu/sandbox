package win.devhu.testing.pippo

import ro.pippo.core.Application

/**
 * Demo for Basic Application
 */
class BasicApplication : Application(){
    override fun onInit() {
        //send text as response
        GET("/"){ it.send("Hello,Pippo!") }
        //send json as response
        GET("/json"){
            val contact = Contact(1,"SanZhang","185xxxx2001","GZ@China")
            it.json().send(contact)
        }
        // send xml as response
        GET("/xml"){
            val contact = Contact(2,"SiLi","185xxxx2001","GZ@China")
            it.xml().send(contact)
        }
        //send template as response
        GET("/template"){
            it.setLocal("greeting", "Just for test!")
            it.render("index")
        }
    }

}

//contact as a simple POJO for test
data class Contact (val id: Int = 0, val name: String? = null, val phone: String? = null, val address: String? = null) {
    override fun toString(): String {
        return "Contact(id=$id, name=$name, phone=$phone, address=$address)"
    }
}