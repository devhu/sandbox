package win.devhu.testing.pippo

import org.slf4j.LoggerFactory
import ro.pippo.controller.Interceptor
import ro.pippo.core.Pippo
import ro.pippo.core.route.RouteContext
import ro.pippo.core.route.RouteHandler
import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

class PippoApp

private  val logger = LoggerFactory.getLogger(PippoApp::class.java)

fun main(args: Array<String>) {
//    val pippo =  Pippo(BasicApplication())  //load BasicApplication
    val pippo =  Pippo(ControllerApp())  //load Controller App
    pippo.start()
}

/**
 * user-defined logging annotation
 */
class LoggingHandler : RouteHandler<RouteContext>{

    override fun handle(routeContext: RouteContext) {
        logger.debug(("LoggingHandler.handle"))
        val route = routeContext.route
        logger.debug("route : $route,Method : ${routeContext.requestMethod},URI : ${routeContext.requestUri}")
        routeContext.next()
    }
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE, ElementType.METHOD)
@Interceptor(LoggingHandler::class)
annotation class Logging
