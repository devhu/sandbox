package win.devhu.testing.pippo.common

import ro.pippo.controller.*
import win.devhu.testing.pippo.Contact
import win.devhu.testing.pippo.Logging

class ContactsController : Controller(){

    private val contactService = ContactServiceImpl()

    @GET("/api")
    @Named("api.getAll")
    @Produces(Produces.JSON)
    @NoCache
    @Logging
    fun getAll(): List<Contact> {
        return contactService.getContacts()
    }
}