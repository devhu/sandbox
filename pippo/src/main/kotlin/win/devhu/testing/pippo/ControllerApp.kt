package win.devhu.testing.pippo

import ro.pippo.controller.ControllerApplication
import ro.pippo.core.route.RouteContext
import ro.pippo.core.route.RouteHandler
import win.devhu.testing.pippo.common.ContactsController

class ControllerApp : ControllerApplication() {
    override fun onInit() {

        //add route
        GET("/") { it.send("Hello") }

        addControllers(ContactsController::class.java)

    }

}
