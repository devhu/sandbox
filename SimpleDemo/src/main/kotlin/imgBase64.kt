package win.devhu.testing.simple

import java.io.File
import java.util.*

fun main(args : Array<String>) {
    val imagePath = "tmp/test.jpg"

    //图片转BASE64格式
    val base64ImageString = encoder(imagePath)
    println("Base64ImageString = $base64ImageString")

    //BASE64格式图片转图片文件
    decoder(base64ImageString, "tmp/decoderimage.png")

    println("完成！")
}

fun encoder(filePath: String): String{
    val bytes = File(filePath).readBytes()
    val base64 = Base64.getEncoder().encodeToString(bytes)
    return base64
}

fun decoder(base64Str: String, pathFile: String): Unit{
    val imageByteArray = Base64.getDecoder().decode(base64Str)
    File(pathFile).writeBytes(imageByteArray)
}