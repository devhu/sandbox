package win.devhu.testing.simple

import java.time.LocalDateTime
import java.time.Month
import java.util.*

fun main(args: Array<String>) {
    val currentDateTime = LocalDateTime.now()
    println(currentDateTime)
    /*
        -> 2018-12-24T12:34
     */

    val year = currentDateTime.getYear()
    println(year)
    /*
         -> 2018
     */

    val month = currentDateTime.getMonth()
    println(month)
    /*
         -> JANUARY
     */

    val firstMonthOfQuarter = month.firstMonthOfQuarter()
    println(firstMonthOfQuarter)
    /*
         -> JANUARY
     */

    val day = currentDateTime.getDayOfMonth()
    println(day)
    /*
         -> 30
     */

    val hour = currentDateTime.getHour()
    println(hour)
    /*
         -> 22
     */

    val minute = currentDateTime.getMinute()
    println(minute)
    /*
         -> 25
     */

    val seconds = currentDateTime.getSecond();
    println(seconds)
    /*
         -> 26
     */

    val XMas = LocalDateTime.of(2018, Month.DECEMBER, 24, 12, 34)
    println(XMas)
    /*
         -> 2018-12-24T12:34
     */

    // 精确到毫秒
    // 获取当前时间戳
    println(System.currentTimeMillis())
    println(Calendar.getInstance().getTimeInMillis())
    println(Date().getTime())
}