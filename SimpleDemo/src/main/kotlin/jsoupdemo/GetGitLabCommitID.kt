package win.devhu.testing.simple.jsoupdemo

import org.jsoup.Jsoup

fun main(args: Array<String>){
    val urls = mapOf("m.vip.com" to "http://gitlab.tools.vipshop.com/wap/wap/commits/master",
            "ms2-m.vipstatic.com" to "http://gitlab.tools.vipshop.com/ms2m/ms2m/commits/master",
            "mcheckout.vip.com" to "http://gitlab.tools.vipshop.com/wap/mcheckout/commits/master",
            "m-admin.vip.vip.com" to "http://gitlab.tools.vipshop.com/wapadmin/wapadmin/commits/master",
            "wechat.api.vip.com" to "http://gitlab.tools.vipshop.com/weixin/wechat.api.vip.com/commits/master"
    )

    println("--------------------------------------------")
    urls.forEach{ (k,v) -> getCommitID("$k","$v")}
    println("--------------------------------------------")
}

fun getCommitID(doname:String,pageUrl:String){
    var cookies = HashMap<String,String>()
    cookies.put("_gitlab_session","940cf6b9b17a20c7222e7dbabcfbd586")   //设置cookies

    val doc = Jsoup.connect(pageUrl).cookies(cookies).get()
    val commit_short_lines = doc.select("#commits-list div.pull-right a")
    for(line in commit_short_lines){
        println("【${doname}】最新版本号：${line.text()}")
        break
    }
}