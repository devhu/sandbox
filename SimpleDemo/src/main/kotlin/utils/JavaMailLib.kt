package win.devhu.testing.simple.utils

import java.util.*
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

fun sendByJavaMail() {
    val prop = Properties()
    prop.put("mail.smtp.host", "smtp-mail.outlook.com")
    prop.put("mail.smtp.port", "587")
    prop.put("mail.smtp.auth", "true")
    prop.put("mail.smtp.starttls.enable", "true")

    val session = Session.getDefaultInstance(prop, object: Authenticator(){
        override fun getPasswordAuthentication(): PasswordAuthentication {
            return PasswordAuthentication("thiszr@outlook.com", "ZGV2aHUud2lu".sslmail())
        }
    })
    val message = MimeMessage(session)
    message.setFrom("thiszr@outlook.com")
    message.setRecipient(Message.RecipientType.TO, InternetAddress("devhu@foxmail.com"))
    message.setSubject("Kotlinからの送信テスト(SMTP/JavaMail)", "UTF-8")
    message.setText("Kotlinから送った\n系统邮件，请勿回复！", "UTF-8")       //纯文本
//    message.setContent("<h1>This is actual message</h1>", "text/html")  //html格式
    Transport.send(message)
}

fun String.sslmail(): String {
    return if (this.isEmpty()) { "" }
    else {
        String(Base64.getDecoder().decode(this))
    }
}

fun main(args: Array<String>) {
    sendByJavaMail()
}