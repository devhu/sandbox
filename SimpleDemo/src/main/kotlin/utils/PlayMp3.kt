package win.devhu.testing.simple.utils

import javax.sound.sampled.Clip
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.AudioInputStream

class PlayMp3
fun main(args: Array<String>) {
    val audioInputStream: AudioInputStream = AudioSystem.getAudioInputStream(
            PlayMp3::class.java.classLoader.getResource("7b1388a5730f.wav"))
    val clip: Clip = AudioSystem.getClip()
    clip.open(audioInputStream)
    clip.start()
    while (!clip.isRunning())
        Thread.sleep(5)
    while (clip.isRunning())
        Thread.sleep(5)
    clip.close()
}
