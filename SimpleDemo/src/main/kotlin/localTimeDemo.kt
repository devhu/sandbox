package win.devhu.testing.simple

import java.time.LocalTime

fun main(args: Array<String>) {

    //17:41:43.171
    val currentTime = LocalTime.now()
    println(currentTime)

    // it depends on currentTime >> 17:41:43.171 -> 60558
    val secondOfDay = currentTime.toSecondOfDay()
    println(secondOfDay)

    val specHMS = LocalTime.of(12, 34, 56)
    println(specHMS)
    /*
         -> 12:34:56
     */

    val parseHMS = LocalTime.parse("12:34:56")
    println(parseHMS)
    /*
         -> 12:34:56
     */

    val secondsOfDay = LocalTime.ofSecondOfDay(12345)
    println(secondsOfDay)
    /*
         12345th second of a day is [03:25:45]
         -> 03:25:45
     */
}