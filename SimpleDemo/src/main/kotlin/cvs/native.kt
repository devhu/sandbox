package win.devhu.testing.simple.cvs

import java.io.FileWriter
import java.io.IOException
import java.util.*

private val CSV_HEADER = "id,name,address,age"

fun main(args: Array<String>) {

    val customers = Arrays.asList(
            Customer("1", "Jack Smith", "Massachusetts", 23),
            Customer("2", "Adam Johnson", "New York", 27),
            Customer("3", "Katherin Carter", "Washington DC", 26),
            Customer("4", "Jack London", "Nevada", 33),
            Customer("5", "Jason Bourne", "California", 36))

    var fileWriter: FileWriter? = null

    val csvPath = "src/main/resources/customer.csv"

    try {
        fileWriter = FileWriter(csvPath)
        fileWriter.append(CSV_HEADER)
        fileWriter.append('\n')

        for (customer in customers) {
            fileWriter.append(customer.id)
            fileWriter.append(',')
            fileWriter.append(customer.name)
            fileWriter.append(',')
            fileWriter.append(customer.address)
            fileWriter.append(',')
            fileWriter.append(customer.age.toString())
            fileWriter.append('\n')
        }

        println("Write CSV successfully!")
    } catch (e: Exception) {
        println("Writing CSV error!")
        e.printStackTrace()
    } finally {
        try {
            fileWriter!!.flush()
            fileWriter.close()
        } catch (e: IOException) {
            println("Flushing/closing error!")
            e.printStackTrace()
        }
    }
}