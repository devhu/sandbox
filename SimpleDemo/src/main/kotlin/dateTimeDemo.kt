package win.devhu.testing.simple

import java.time.LocalDate

fun main(args:Array<String>){
    //获取当前日期
    val currentDate = LocalDate.now()
    println(currentDate)

    //判断是星期几
    val dayOfWeek = currentDate.getDayOfWeek()
    println(dayOfWeek)

    //判断是否是闰年
    val isLeapYear = currentDate.isLeapYear ()
    println(isLeapYear)

}