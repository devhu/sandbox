package win.devhu.testing.simple.exposedemo

import org.apache.poi.ss.usermodel.DataFormatter
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.joda.time.DateTime
import java.io.File
import java.text.SimpleDateFormat

/**
 * @Date: 2018/9/24
 * @Description: 导入Excel数据，Excel数据处理
 */

//data bean
data class ItemInfo(val saletime: DateTime,
                    val item: String,
                    val price: String,
                    val remark: String)

class ParseExcelMoneyData(file: File) {
    private val wb: Workbook
    private val formatter: DataFormatter
    private val dateFormatter: SimpleDateFormat
    val items: MutableList<ItemInfo>

    init {
        items = ArrayList()
//        wb = WorkbookFactory.create(file)   //如果是.xls格式
        wb = XSSFWorkbook(file) //.xlsx格式
        formatter = DataFormatter()
        dateFormatter = SimpleDateFormat("yyyy/MM/dd")
    }

    fun parseItemsInfo(sheetIndex: Int = 0) {
        val sheet = wb.getSheetAt(sheetIndex)
        for (row_index in 1..sheet.lastRowNum) {//第0行是表单，我们不用，所以下标从一开始
            val row = sheet.getRow(row_index)
            val item = formatter.formatCellValue(row.getCell(0))
            val price = formatter.formatCellValue(row.getCell(1))
            val saletime = DateTime(dateFormatter.parse(formatter.formatCellValue(row.getCell(2))).time)
            val remark = formatter.formatCellValue(row.getCell(3))
            items.add(ItemInfo(saletime = saletime, item = item,
                    price = price, remark = remark))
        }
    }
}