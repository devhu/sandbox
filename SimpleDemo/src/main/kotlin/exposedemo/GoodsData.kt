package win.devhu.testing.simple.exposedemo

import org.jetbrains.exposed.sql.Table

/**
 * @Date: 2018/9/24
 * @Description:
 */

//使用单例创建数据表的模版
object GoodsData : Table() {
    val item = varchar("item",50).primaryKey()
    val price = varchar("price", 8)
    val saletime = datetime("saletime")
    val remark = text("remark")
    val status = text("status")
}

