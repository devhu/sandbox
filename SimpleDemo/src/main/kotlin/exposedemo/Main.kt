package win.devhu.testing.simple.exposedemo

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

import java.util.*

/**
 * @Date: 2018/9/24
 * @Description: 学习 Kotlin  ORM 框架 win.devhu.testing.simple.exposedemo 使用
 */

class Main

fun main(args: Array<String>) {
    val (user, password) = with(Scanner(System.`in`)) {//获取用户名和密码
        println("enter user and password :")
        return@with (next() to next())
    }
    val db = Database.connect(user = user,//连接数据库
            password = password,
            url = "jdbc:mysql://localhost/ktordb?serverTimezone=UTC&useUnicode=true&characterEncoding=utf8&useSSL=false",
            driver = "com.mysql.cj.jdbc.Driver")

    val file_path = "tmp/goodslists.xlsx"

    val parse = ParseExcelMoneyData(File(file_path))    //根目錄
//    val parse = ParseExcelMoneyData(Main::class.java.classLoader.getResourceAsStream(excelPath))    //resources 目錄

    parse.parseItemsInfo()//解析Excel
//  parse.items.map { println(it) }//打印集合
    val itemInfos = parse.items
    transaction(db) {//操纵数据库
        addLogger(StdOutSqlLogger)//添加日志，输出到标准输出
        SchemaUtils.createMissingTablesAndColumns(GoodsData)//创建表
        GoodsData.batchInsert(itemInfos) {//（batchInsert函数参数是一个可迭代的对象）插入数据
            this[GoodsData.saletime] = it.saletime
            this[GoodsData.item] = it.item
            this[GoodsData.price] = it.price
            this[GoodsData.remark] = it.remark
        }
    }
}