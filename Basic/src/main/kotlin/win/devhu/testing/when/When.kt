package win.devhu.testing.`when`

//kotlin 的 when 语句相当于其他语言中switch...case 语句
fun main(args: Array<String>) {
    printWeekDay(1);
    printWeekDay(2);
    printWeekDay(3);
    printWeekDay(4);
    printWeekDay(5);
    printWeekDay(6);
    printWeekDay(7);
    printWeekDay(12);
}

fun printWeekDay(n: Int){
    when (n) {
        1 -> {
            println("Sunday")
        }
        2 -> {
            println("Monday")
        }
        3 -> {
            println("Tuesday")
        }
        4 -> {
            println("Wednesday")
        }
        5 -> {
            println("Thursday")
        }
        6 -> {
            println("Friday")
        }
        7 -> {
            println("Saturday")
        }
        else -> { // Note the block
            println("Invalid value")
        }
    }


}