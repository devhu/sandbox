package win.devhu.testing.string

import java.io.File
import java.io.InputStream
import java.nio.charset.Charset

//kotlin 读取文件相关操作

fun main(args: Array<String>) {
    val file = File("tmp/contents.txt")
    print ("文件路径：${file.absolutePath}\n")

    //readText
    print("===使用readText方法===：\n\n")
    var content1:String = file.readText()
    println(content1)

    //bufferedReader
    print("====使用bufferedReader方法===:\n\n")
    val bufferedReader = file.bufferedReader()
    val text:List<String> = bufferedReader.readLines()
    for(line in text){
        println(line)
    }

    //eachline语句
    print("===使用forEachLine方法===:\n\n")
    file.forEachLine { println(it) }

    //inputStream 语句
    print("===使用inputStream方法===:\n\n")
    var ins: InputStream = file.inputStream()
    // read contents of IntputStream to String
    var content = ins.readBytes().toString(Charset.defaultCharset())
    println(content)

    //readBytes
    print("===使用readBytes方法===:\n\n")
    var bytes:ByteArray = file.readBytes()
    for(byte in bytes){
        print(byte.toChar())
    }

    //readLines
    print("===使用readLines方法===:\n\n")
    var lines:List<String> = file.readLines()
    for(line in lines){
        println(line)
    }
}