package win.devhu.testing.string

import java.io.File

/**
 * Kotlin Example to read contents of file line by line
 */
fun main(args: Array<String>) {
    val fileName :String = "tmp/contents.txt"
    var i :Int = 1
    // using extension function readLines
    File(fileName).readLines().forEach {
        print((i++))
        println(": "+it)
    }
}