package win.devhu.testing.string

/**
 * Kotlin example to compare two strings
 */

fun main(args: Array<String>) {
    var a: String = "apple"
    var b: String = "apple"
    var result = a.compareTo(b)
    if(result==0){
        println("Strings '$a' and '$b' are equal.")
    } else if(result < 0){
        println("'$a' is less than '$b' lexically.")
    } else{
        println("'$a' is less than '$b' lexically.")
    }

    b = "banana"
    result = a.compareTo(b)
    if(result==0){
        println("Strings '$a' and '$b' are equal.")
    } else if(result < 0){
        println("'$a' is less than '$b' lexically.")
    } else{
        println("'$a' is less than '$b' lexically.")
    }

    // passing ignoreCase to compareTo
    a = "appLE"
    b = "aPple"
    println("\nIgnoring Case...")
    result = a.compareTo(b, true)  // ignoreCase = true
    if(result==0){
        println("Strings '$a' and '$b' are equal.")
    } else if(result < 0){
        println("'$a' is less than '$b' lexically.")
    } else{
        println("'$a' is less than '$b' lexically.")
    }
}
