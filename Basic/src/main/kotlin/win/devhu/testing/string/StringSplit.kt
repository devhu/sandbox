package win.devhu.testing.string


/**
 * Kotlin example to split string to lines
 */

fun main(args: Array<String>) {
    // string to be split to lines
    var str: String = "Kotlin Tutorial.\nLearn Kotlin Programming with Ease.\rLearn Kotlin Basics."

    // splitting string using lines() function
    var lines = str.lines()

    // printing lines
    lines.forEach { println(it) }
}

