package win.devhu.testing.loops

//Kotlin 循环语句总结
fun main(args: Array<String>) {

    //for 循环
    var daysOfWeek = listOf("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
    for(day in daysOfWeek){
        println(day)
    }

    //for range 功能
    for(i in 1..4){
        println(i)
    }

    // for 迭代的元素的索引
    for((index,day) in daysOfWeek.withIndex()){
        println("$index :$day")
    }

    //forEach 语句
    daysOfWeek.forEach{
        println(it)
    }

    //repeat 语句
    repeat(4) {
        println("Hello World!")
    }

    //while 循环语句
    var i: Int = 1
    while(i<4){
        println(i)
        i=i+1
    }

    i = 1
    //while + break
    while(true){
        println(i)
        i=i+1
        if(i>4)
            break;
    }

    i = 1
    //do...while语句
    do{
        println(i)
        i=i+1
    }while(i<0)
}