package win.devhu.testing.data

//Kotlin 数据类
import java.util.ArrayList

fun main(args: Array<String>) {
    var books: ArrayList<Book> = ArrayList()
    books.add(Book("The Last Sun", 250))
    books.add(Book("Three Friends", 221))
    for(book in books)
        println(book)
}

data class Book(val name: String = "", val price: Int = 0)