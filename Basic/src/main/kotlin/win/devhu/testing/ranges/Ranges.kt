package win.devhu.testing.ranges

/**
 * Kotlin Ranges could be found in three forms.
    m..n
    n downTo m
    m until n
 */

fun main(args: Array<String>) {
    //m..n
    for(i in 2..4){
        println(i)
    }

    //在if 语句中使用
    var r = 3
    if(r in 2..4){
        println(r.toString() + " is in the range 2..4")
    }

    //n downTo m
    for(i in 4 downTo 2){
        println(i)
    }

    //m until n 相比于 m..n 区别在于是不包含 n 的
    for(i in 2 until 4){
        println(i)
    }
}
