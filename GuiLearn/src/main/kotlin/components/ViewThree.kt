package win.devhu.testing.gui.components

import tornadofx.*

class ViewThree : View() {
    val controller: MyControllerThree by inject() //注入

    override val root = vbox {
        val textfield = textfield()
        button("Update text") {
            action {
                runAsync {
                    controller.loadText()   //异步任务
                } ui { loadedText ->
                    textfield.text = loadedText
                }
            }
        }
    }
}

class MyControllerThree: Controller() {
    fun loadText(): String {
        val timezone = System.currentTimeMillis()
        Thread.sleep(1000)  //此处等待3s
        return timezone.toString()
    }
}