package win.devhu.testing.gui.components

import javafx.beans.property.SimpleStringProperty
import tornadofx.*

class ViewOne : View() {
    val controller: MyController by inject()
    val input = SimpleStringProperty()

    override val root = form {
        fieldset {
            field("输入") {
                textfield(input)
            }

            button("提交") {
                action {
                    controller.writeToDb(input.value)
                    input.value = ""
                }
            }
        }
    }
}

//Controller 示例

    class MyController: Controller() {
        fun writeToDb(inputValue: String) {
            println("Writing $inputValue to database!")
        }
    }