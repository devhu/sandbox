package win.devhu.testing.gui.components

import javafx.scene.chart.CategoryAxis
import javafx.scene.chart.NumberAxis
import tornadofx.*

class ViewFour : View() {
    override val root = vbox {

        title = "图表示例"

        piechart("Imported Fruits") {
            data("Grapefruit", 12.0)
            data("Oranges", 25.0)
            data("Plums", 10.0)
            data("Pears", 22.0)
            data("Apples", 30.0)
        }

        barchart("Stock Monitoring, 2010", CategoryAxis(), NumberAxis()) {
            series("Portfolio 1") {
                data("Jan", 23)
                data("Feb", 14)
                data("Mar", 15)
            }
            series("Portfolio 2") {
                data("Jan", 11)
                data("Feb", 19)
                data("Mar", 27)
            }
        }

    }
}

