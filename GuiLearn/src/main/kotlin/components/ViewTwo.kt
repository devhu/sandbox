package win.devhu.testing.gui.components

import javafx.collections.FXCollections
import tornadofx.*

class ViewTwo : View() {
    val controller: MyControllerTwo by inject() //注入

    override val root = vbox {
        label("My items")
        listview(controller.values)
    }
}

//您还可以使用控制器向View提供数据
class MyControllerTwo: Controller() {
    val values = FXCollections.observableArrayList("Alpha","Beta","Gamma","Delta")
}