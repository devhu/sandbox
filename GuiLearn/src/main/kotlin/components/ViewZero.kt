package win.devhu.testing.gui.components

import tornadofx.*

//嵌入其他视图
class ViewZero: View() {
    // 显示查找引用
    val topView = find(TopView::class)
    // 懒加载
    val bottomView: BottomView by inject()

    override val root = borderpane {
        top = topView.root
        bottom = bottomView.root
    }
}


class TopView: View() {
    override val root = label("Top View")
}

class BottomView: View() {
    override val root = label("Bottom View")
}