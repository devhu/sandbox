package win.devhu.testing.gui.BasicControls

import tornadofx.*

class DemoLayout : View() {

    override val root =  vbox {
        button("Button 1").setOnAction {
            println("Button 1 Pressed")
        }
        button("Button 2").setOnAction {
            println("Button 2 Pressed")
        }

        hbox {
            button("Button 1").setOnAction {
                println("Button 1 Pressed")
            }
            button("Button 2").setOnAction {
                println("Button 2 Pressed")
            }
        }

        flowpane {
            for (i in 1..100) {
                button(i.toString()) {
                    setOnAction { println("You pressed button $i") }
                }
            }
        }
    }


}