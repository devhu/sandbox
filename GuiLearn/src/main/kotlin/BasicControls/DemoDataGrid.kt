package win.devhu.testing.gui.BasicControls

import javafx.scene.paint.Color
import tornadofx.*

class DemoDataGrid : View() {

    override val root = vbox {

        val kittens = listOf("https://i.imgur.com/DuFZ6PQb.jpg", "https://i.imgur.com/o2QoeNnb.jpg",
        "https://i.imgur.com/DuFZ6PQb.jpg", "https://i.imgur.com/o2QoeNnb.jpg",
        "https://i.imgur.com/DuFZ6PQb.jpg", "https://i.imgur.com/o2QoeNnb.jpg",
        "https://i.imgur.com/DuFZ6PQb.jpg", "https://i.imgur.com/o2QoeNnb.jpg") // more items here
        datagrid(kittens) {
            cellCache {
                imageview(it)
            }
        }


        val numbers = (1..10).toList()
        datagrid(numbers) {
            cellHeight = 75.0
            cellWidth = 75.0
            multiSelect = true
            cellCache {
                stackpane {
                    circle(radius = 25.0) {
                        fill = Color.FORESTGREEN
                    }
                    label(it.toString())
                }
            }
        }


    }

}