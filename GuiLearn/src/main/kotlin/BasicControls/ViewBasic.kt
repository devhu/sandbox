package win.devhu.testing.gui.BasicControls

import javafx.application.Platform
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.scene.paint.Color
import javafx.scene.text.Font
import tornadofx.*
import java.time.LocalDate
import kotlin.concurrent.thread

class ViewBasic : View() {


     override val root = vbox {
         button("Press Me") {
             textFill = Color.RED
             action {
                 println("Button pressed!")
             }
         }

         label("Lorem ipsum") {
             textFill = Color.BLUE
         }

         textfield("Input something") {
             textProperty().addListener { obs, old, new ->
                 println("You typed: " + new)
             }
         }

         passwordfield("password123") {
             requestFocus()
         }

         val booleanProperty = SimpleBooleanProperty()       //用于绑定checkbox 状态 booleanProperty.value
         checkbox("Admin Mode",booleanProperty) {
             action { println(isSelected) }
         }
//         println("状态${booleanProperty.value}")

         val texasCities = FXCollections.observableArrayList("Austin",
                 "Dallas","Midland", "San Antonio","Fort Worth")
//         combobox<String> {
//             items = texasCities
//         }
//         combobox(values = texasCities)       //another style
         val selectedCity = SimpleStringProperty()      //通过变量获取选中的值
         combobox(selectedCity, texasCities)
//         println(selectedCity.value)        //获取选中的值

         //切换按钮
         togglebutton("OFF") {
             action {
                 text = if (isSelected) "ON" else "OFF"
             }
         }

         togglebutton {
             val stateText = selectedProperty().stringBinding {
                 if (it == true) "开启" else "关闭"
             }
             textProperty().bind(stateText)     //通过绑定方式设置显示文本
         }

//         val toggleGroup = ToggleGroup()
//         togglebutton("YES", toggleGroup)
//         togglebutton("NO", toggleGroup)
//         togglebutton("MAYBE", toggleGroup)
         radiobutton("Power User Mode") {
             action {
                 println("Power User Mode: $isSelected")
             }
         }

//         radiobutton组类似于多个选项单选
//         val toggleGroup = ToggleGroup()
//         radiobutton("Employee", toggleGroup)
//         radiobutton("Contractor", toggleGroup)
//         radiobutton("Intern", toggleGroup)

//         日期选择器
         val dateProperty = SimpleObjectProperty<LocalDate>()   //参数绑定
         datepicker(dateProperty) {     //参数绑定
             value = LocalDate.now()
         }
//         println(dateProperty.value)

         textarea("Type memo here") {
             selectAll()
         }

         //进度条
         progressbar(0.5)
        //动态进度条模拟
         progressbar {
             thread {
                 for (i in 1..100) {
                     Platform.runLater { progress = i.toDouble() / 100.0 }
                     Thread.sleep(100)
                 }
             }
         }

         progressindicator() {
             thread {
                 for (i in 1..100) {
                     Platform.runLater { progress = i.toDouble() / 100.0 }
                     Thread.sleep(100)
                 }
             }
         }

         scrollpane {//滚动条
             imageview("github.png")
         }

         //超链接
         hyperlink("关于").action { println("点击\"关于\"链接") }

         //类似于label
         text("Veni\nVidi\nVici") {
             fill = Color.PURPLE
             font = Font(20.0)
         }

         textflow {
             text("Tornado") {
                 fill = Color.PURPLE
                 font = Font(20.0)
             }
             text("FX") {
                 fill = Color.ORANGE
                 font = Font(28.0)
             }
         }

         button("Commit") {
             tooltip("Writes input to the database")
             font = Font.font("Verdana")
         }

         shortpress { println("Activated on short press") }     //短按
         longpress { println("Activated on long press") }       //长按

     }
}