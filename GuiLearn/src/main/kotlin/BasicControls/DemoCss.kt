package win.devhu.testing.gui.BasicControls

import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import tornadofx.*

class DemoCss : View() {

    override val root = vbox {
        button("Press Me") {
            style {
                fontWeight = FontWeight.EXTRA_BOLD
                borderColor += box(
                        top = Color.RED,
                        right = Color.DARKGREEN,
                        left = Color.ORANGE,
                        bottom = Color.PURPLE
                )
                rotate = 45.deg
            }
            setOnAction { println("You pressed the button") }
        }

    }
}
