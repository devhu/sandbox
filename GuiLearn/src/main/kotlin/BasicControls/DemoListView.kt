package win.devhu.testing.gui.BasicControls

import javafx.scene.control.SelectionMode
import javafx.scene.paint.Color
import tornadofx.*
import java.time.LocalDate
import java.time.Period

class DemoListView : View() {

    override val root = vbox {
//        listview<String> {
//            items.add("Alpha")
//            items.add("Beta")
//            items.add("Gamma")
//            items.add("Delta")
//            items.add("Epsilon")
//            selectionModel.selectionMode = SelectionMode.MULTIPLE
//        }

        val greekLetters = listOf("Alpha","Beta",
                "Gamma","Delta","Epsilon").observable()
        listview(greekLetters) {
            selectionModel.selectionMode = SelectionMode.MULTIPLE
        }

        val persons = listOf(
                Person(1,"Samantha Stuart",LocalDate.of(1981,12,4)),
                Person(2,"Tom Marks",LocalDate.of(2001,1,23)),
                Person(3,"Stuart Gills",LocalDate.of(1989,5,23)),
                Person(3,"Nicole Williams",LocalDate.of(1998,8,11))
        ).observable()

        tableview(persons) {
            readonlyColumn("ID",Person::id)
            readonlyColumn("Name", Person::name)
            readonlyColumn("Birthday", Person::birthday)
            readonlyColumn("Age",Person::age).cellFormat {
                text = it.toString()
                style {         //设置表格样式
                    if (it < 18) {
                        backgroundColor += c("#8b0000")
                        textFill = Color.WHITE
                    }
                }
            }
        }



    }

}

class Person(val id: Int, val name: String, val birthday: LocalDate) {
    val age: Int get() = Period.between(birthday, LocalDate.now()).years        //计算年龄
}
