package win.devhu.testing.gui.BasicControls

import javafx.scene.control.Button
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import tornadofx.*

class ViewButton : View() {

    override val root = VBox()

    init {
        with(root) {
            this += Button("Press Me").apply {
                textFill = Color.RED
                action { println("Button pressed!") }
            }

//            简化写法
            button("Press Me Once more") {
                textFill = Color.RED
                action { println("Button pressed once more!") }
            }
        }
    }
}