package win.devhu.testing.gui.BasicControls

import javafx.scene.control.TextField
import tornadofx.*

class ViewHbox : View() {

    var firstNameField: TextField by singleAssign()     //singleAssign 保证属性只分配一次
    var lastNameField: TextField by singleAssign()

    override val root = vbox {
        hbox {
            label("First Name")
            firstNameField = textfield()
        }
        hbox {
            label("Last Name")
            lastNameField = textfield()
        }
        button("LOGIN") {
            useMaxWidth = true
            action {
                println("Logging in as ${firstNameField.text} ${lastNameField.text}")
            }
        }
    }
}