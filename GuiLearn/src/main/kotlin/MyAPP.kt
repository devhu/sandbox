package win.devhu.testing.gui

import javafx.application.Application.launch
import tornadofx.App

class AppDemo:App(EventBusTestView::class)


fun main(args: Array<String>) {
    launch(AppDemo::class.java,*args)
}

