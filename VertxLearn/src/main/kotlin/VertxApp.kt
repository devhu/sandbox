import io.vertx.core.Vertx
import io.vertx.core.logging.LoggerFactory

class VertxApp

private val log = LoggerFactory.getLogger(VertxApp::class.java)

fun main(args: Array<String>){

    val vertx = Vertx.vertx()
    var server = vertx.createHttpServer()

    server.requestHandler { request ->
        request.response().end("hello world!")
    }

    server.listen(8090) { res ->
        if (res.succeeded()) {
            log.info("Server is now listening!")
        } else {
            log.info("Failed to bind!")
        }
    }
}