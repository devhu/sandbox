import io.vertx.core.Vertx
import io.vertx.core.logging.LoggerFactory
import io.vertx.ext.jdbc.JDBCClient
import io.vertx.kotlin.core.json.JsonObject
import io.vertx.kotlin.core.json.get


class TestDB

    private val log = LoggerFactory.getLogger(TestDB::class.java)
    private val vertx = Vertx.vertx()

    fun main(args: Array<String>) {
        val config = JsonObject()
                .put("url","jdbc:mysql://10.189.108.51:3306/vip_ulssingin")
                .put("driver_class","com.mysql.jdbc.Driver")
                .put("user","vipshop")
                .put("password","dWwkv%+!1cQf\$FNy")
        val client = JDBCClient.createNonShared(vertx,config)

        client.getConnection { res ->
            if (res.succeeded()) {

                val connection = res.result()
                val sql = "select active_code from svip_card where svip_card_batch_id = 592 and `status`=1 limit 1"
                connection.query(sql) { it ->
                    if (it.succeeded()) {
                        val rs = it.result()
                        log.info("激活码查询结果是：${rs.rows[0].get<String>("active_code")}")
                        //三步关闭
                        connection.close()
                        client.close()
                        vertx.close()
                    }
                }
            } else {
                log.info("查询失败")
            }
        }


    }

