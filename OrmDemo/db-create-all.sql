create table customer (
  id                            bigint auto_increment not null,
  name                          varchar(255) not null,
  version                       bigint not null,
  when_modified                 timestamp not null,
  when_created                  timestamp not null,
  constraint pk_customer primary key (id)
);

create table orders (
  id                            bigint auto_increment not null,
  order_date                    date not null,
  customer_id                   bigint not null,
  version                       bigint not null,
  when_modified                 timestamp not null,
  when_created                  timestamp not null,
  constraint pk_orders primary key (id)
);

create index ix_orders_customer_id on orders (customer_id);
alter table orders add constraint fk_orders_customer_id foreign key (customer_id) references customer (id) on delete restrict on update restrict;

