alter table orders drop constraint if exists fk_orders_customer_id;
drop index if exists ix_orders_customer_id;

drop table if exists customer;

drop table if exists orders;

