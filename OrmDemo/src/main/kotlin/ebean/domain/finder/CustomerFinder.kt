package ebean.domain.finder

import ebean.domain.Customer
import io.ebean.Finder

open class CustomerFinder : Finder<Long, Customer>(Customer::class.java)
