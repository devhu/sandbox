package ebean.domain

import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name="orders")
class Order(
        var orderDate: LocalDate,
        @ManyToOne
        var customer: Customer
): BaseDomain()