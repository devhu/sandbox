package ebean.domain

import ebean.domain.finder.CustomerFinder
import javax.persistence.Entity

@Entity
class Customer(
        var name: String
) : BaseDomain(){
    companion object Find : CustomerFinder()
}