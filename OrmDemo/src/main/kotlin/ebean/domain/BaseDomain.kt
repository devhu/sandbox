package ebean.domain

import io.ebean.Model
import io.ebean.annotation.WhenCreated
import io.ebean.annotation.WhenModified
import java.time.Instant
import javax.persistence.Id
import javax.persistence.MappedSuperclass
import javax.persistence.Version

@MappedSuperclass
open class BaseDomain : Model() {

    @Id
    var id: Long = 0

    @Version
    var version: Long = 0

    @WhenModified
    lateinit var whenModified: Instant

    @WhenCreated
    lateinit var whenCreated: Instant

}