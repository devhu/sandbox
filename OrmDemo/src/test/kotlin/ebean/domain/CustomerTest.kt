package ebean.domain

import org.junit.Test

class CustomerTest{
    @Test
    fun insert_update_delete() {

        val customer = Customer("Hello entity bean")
        // insert
        customer.save()

        // update
        customer.name = "Goodbye"
        customer.save()

        // delete the bean
        customer.delete()
    }


}